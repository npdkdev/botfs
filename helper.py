from json import dumps
from dataclasses import dataclass

@dataclass
class Dpo:
    c: dict
    pay: dict
    s: dict
    item: dict
    shipping: dict
def place_orderv2(timestamp,data: Dpo):
    return dumps({
        "client_id": 0,
        "cart_type": 1,
        "timestamp": timestamp,
        "checkout_price_data": {
            "merchandise_subtotal": data.c['price'],
            "shipping_subtotal_before_discount": data.c['shipping_fee'],
            "shipping_discount_subtotal": 0,
            "shipping_subtotal": data.c['shipping_fee'],
            "tax_payable": 0,
            "tax_exemption": 0,
            "custom_tax_subtotal": 0,
            "promocode_applied": None,
            "credit_card_promotion": None,
            "shopee_coins_redeemed": None,
            "group_buy_discount": 0,
            "bundle_deals_discount": None,
            "price_adjustment": None,
            "buyer_txn_fee":  data.c['txn_fee'],
            "buyer_service_fee": data.c['service_fee'],
            "insurance_subtotal": 0,
            "insurance_before_discount_subtotal": 0,
            "insurance_discount_subtotal": 0,
            "vat_subtotal": 0,
            "total_payable": (data.c['price'] + data.c['shipping_fee'] + data.c['txn_fee'] + data.c['service_fee'])
        },
        "order_update_info": {},
        "dropshipping_info": {
            "enabled": False,
            "name": "",
            "phone_number": ""
        },
        "promotion_data": {
            "can_use_coins": False,
            "use_coins": False,
            "platform_vouchers": [],
            "free_shipping_voucher_info": {
                "free_shipping_voucher_id": 0,
                "free_shipping_voucher_code": "",
                "disabled_reason": None,
                "banner_info": {
                    "msg": "",
                    "learn_more_msg": ""
                },
                "required_be_channel_ids": [],
                "required_spm_channels": []
            },
            "highlighted_platform_voucher_type": -1,
            "shop_voucher_entrances": [
                {
                    "shopid": data.s['shopid'],
                    "status": True
                }
            ],
            "applied_voucher_code": None,
            "voucher_code": None,
            "voucher_info": {
                "coin_earned": 0,
                "voucher_code": None,
                "coin_percentage": 0,
                "discount_percentage": 0,
                "discount_value": 0,
                "promotionid": 0,
                "reward_type": 0,
                "used_price": 0
            },
            "invalid_message": "",
            "price_discount": 0,
            "coin_info": {
                "coin_offset": 0,
                "coin_used": 0,
                "coin_earn_by_voucher": 0,
                "coin_earn": 0
            },
            "card_promotion_id": None,
            "card_promotion_enabled": False,
            "promotion_msg": ""
        },
        "selected_payment_channel_data": {
            "version": 2,
            "channel_id": data.pay['chid'],
            "channel_item_option_info": data.pay['option'],
        },
        "shoporders": [
            {
                "shop": {
                    "shopid": data.s['shopid'],
                    "shop_name": data.s['name'],
                    "cb_option": data.s['cb_option'],
                    "is_official_shop": data.s['is_official_shop'],
                    "remark_type": 0,
                    "support_ereceipt": False,
                    "seller_user_id": data.s['userid'],
                    "shop_tag": data.s['shop_tag']
                },
                "items": [
                    {
                        "itemid": data.item['id'],
                        "modelid": data.item['modelid'],
                        "quantity": 1,
                        "item_group_id": None,
                        "insurances": [
                            {
                                "insurance_product_id": "1690597860413811713",
                                "name": "Proteksi Kerusakan +",
                                "description": "Melindungi produkmu dari kerusakan/kerugian total selama 6 bulan.",
                                "product_detail_page_url": "https://insurance.shopee.co.id/product-protection/profile?from=mp_checkout&product_id=1690597860413811713",
                                "premium": 62500000,
                                "premium_before_discount": 62500000,
                                "insurance_quantity": 1,
                                "selected": False
                            }
                        ],
                        "shopid": data.s['shopid'],
                        "shippable": True,
                        "non_shippable_err": "",
                        "none_shippable_reason": "",
                        "none_shippable_full_reason": "",
                        "price": data.c['price'],
                        "name": data.item['name'],
                        "model_name":data.item['model_name'],
                        "add_on_deal_id": 0,
                        "is_add_on_sub_item": False,
                        "is_pre_order": False,
                        "is_streaming_price": False,
                        "image": data.item['image'],
                        "checkout": data.item['checkout'],
                        "categories": [
                            {
                                "catids": data.item['catids']
                            }
                        ],
                        "is_spl_zero_interest": False,
                        "is_prescription": False,
                        "channel_exclusive_info": {
                            "source_id": 0,
                            "token": ""
                        },
                        "offerid": 0,
                        "supports_free_returns": False
                    }
                ],
                "tax_info": {
                    "use_new_custom_tax_msg": False,
                    "custom_tax_msg": "",
                    "custom_tax_msg_short": "",
                    "remove_custom_tax_hint": False
                },
                "tax_payable": 0,
                "shipping_id": 1,
                "shipping_fee_discount": 0,
                "shipping_fee": data.c['shipping_fee'],
                "order_total_without_shipping": data.c['price'],
                "order_total": (data.c['price'] + data.c['shipping_fee']),
                "buyer_remark": "",
                "ext_ad_info_mappings": []
            }
        ],
        "shipping_orders": [
            {
                "shipping_id": 1,
                "shoporder_indexes": [
                    0
                ],
                "selected_logistic_channelid": data.shipping['id'],
                "selected_preferred_delivery_time_option_id": 0,
                "buyer_remark": "",
                "buyer_address_data": {
                    "addressid": data.shipping['address_id'],
                    "address_type": 0,
                    "tax_address": ""
                },
                "fulfillment_info": {
                    "fulfillment_flag": 64,
                    "fulfillment_source": "",
                    "managed_by_sbs": False,
                    "order_fulfillment_type": 2,
                    "warehouse_address_id": 0,
                    "is_from_overseas": False
                },
                "order_total": (data.c['price'] + data.c['shipping_fee']),
                "order_total_without_shipping": data.c['price'],
                "selected_logistic_channelid_with_warning": None,
                "shipping_fee": data.c['shipping_fee'],
                "shipping_fee_discount": 0,
                "shipping_group_description": "",
                "shipping_group_icon": "",
                "tax_payable": 0,
                "is_fsv_applied": False,
                "prescription_info": {
                    "images": None,
                    "required": False,
                    "max_allowed_images": 5
                }
            }
        ],
        "fsv_selection_infos": [],
        "buyer_info": {
            "share_to_friends_info": {
                "display_toggle": False,
                "enable_toggle": False,
                "allow_to_share": False
            },
            "kyc_info": None,
            "checkout_email": ""
        },
        "client_event_info": {
            "is_platform_voucher_changed": False,
            "is_fsv_changed": False
        },
        "disabled_checkout_info": {
            "description": "",
            "auto_popup": False,
            "error_infos": []
        },
        "can_checkout": True,
        "buyer_service_fee_info": {
            "learn_more_url": "https://shopee.co.id/m/biaya-layanan"
        },
        "__raw": {},
        "_cft": [
            1236587
        ],
        "captcha_version": 1,
        "device_info": {}
    })
def place_order(data: Dpo=None):
    return dumps({
        "client_id": 0,
        "cart_type": 1,
        "timestamp": 1670756213,
        "checkout_price_data": {
            "merchandise_subtotal": 1000000000,
            "shipping_subtotal_before_discount": 5000000000,
            "shipping_discount_subtotal": 0,
            "shipping_subtotal": 5000000000,
            "tax_payable": 0,
            "tax_exemption": 0,
            "custom_tax_subtotal": 0,
            "promocode_applied": None,
            "credit_card_promotion": None,
            "shopee_coins_redeemed": None,
            "group_buy_discount": 0,
            "bundle_deals_discount": None,
            "price_adjustment": None,
            "buyer_txn_fee": 100000000,
            "buyer_service_fee": 100000000,
            "insurance_subtotal": 0,
            "insurance_before_discount_subtotal": 0,
            "insurance_discount_subtotal": 0,
            "vat_subtotal": 0,
            "total_payable": 6200000000
        },
        "order_update_info": {},
        "dropshipping_info": {
            "enabled": False,
            "name": "",
            "phone_number": ""
        },
        "promotion_data": {
            "can_use_coins": False,
            "use_coins": False,
            "platform_vouchers": [],
            "free_shipping_voucher_info": {
                "free_shipping_voucher_id": 0,
                "free_shipping_voucher_code": "",
                "disabled_reason": None,
                "banner_info": {
                    "msg": "",
                    "learn_more_msg": ""
                },
                "required_be_channel_ids": [],
                "required_spm_channels": []
            },
            "highlighted_platform_voucher_type": -1,
            "shop_voucher_entrances": [
                {
                    "shopid": 48082125,
                    "status": True
                }
            ],
            "applied_voucher_code": None,
            "voucher_code": None,
            "voucher_info": {
                "coin_earned": 0,
                "voucher_code": None,
                "coin_percentage": 0,
                "discount_percentage": 0,
                "discount_value": 0,
                "promotionid": 0,
                "reward_type": 0,
                "used_price": 0
            },
            "invalid_message": "",
            "price_discount": 0,
            "coin_info": {
                "coin_offset": 0,
                "coin_used": 0,
                "coin_earn_by_voucher": 0,
                "coin_earn": 0
            },
            "card_promotion_id": None,
            "card_promotion_enabled": False,
            "promotion_msg": ""
        },
        "selected_payment_channel_data": {
            "version": 2,
            "option_info": "",
            "channel_id": 8005200,
            "channel_item_option_info": {
                "option_info": "89052004"
            },
            "text_info": {}
        },
        "shoporders": [
            {
                "shop": {
                    "shopid": 48082125,
                    "shop_name": "Leedoo Official Shop",
                    "cb_option": False,
                    "is_official_shop": True,
                    "remark_type": 0,
                    "support_ereceipt": False,
                    "seller_user_id": 48083513,
                    "shop_tag": 1
                },
                "items": [
                    {
                        "itemid": 16414221043,
                        "modelid": 68562461640,
                        "quantity": 1,
                        "item_group_id": None,
                        "insurances": [
                            {
                                "insurance_product_id": "1690597860413811713",
                                "name": "Proteksi Kerusakan +",
                                "description": "Melindungi produkmu dari kerusakan/kerugian total selama 6 bulan.",
                                "product_detail_page_url": "https://insurance.shopee.co.id/product-protection/profile?from=mp_checkout&product_id=1690597860413811713",
                                "premium": 62500000,
                                "premium_before_discount": 62500000,
                                "insurance_quantity": 1,
                                "selected": False
                            }
                        ],
                        "shopid": 48082125,
                        "shippable": True,
                        "non_shippable_err": "",
                        "none_shippable_reason": "",
                        "none_shippable_full_reason": "",
                        "price": 1000000000,
                        "name": "Leedoo Sepatu Pria Sneakers Casual Running Fashion Terbaru MR207",
                        "model_name": "Hitam 42",
                        "add_on_deal_id": 0,
                        "is_add_on_sub_item": False,
                        "is_pre_order": False,
                        "is_streaming_price": False,
                        "image": "39671ff9cb97b19d417ccd7224ddb0e1",
                        "checkout": True,
                        "categories": [
                            {
                                "catids": [
                                    100012,
                                    100064
                                ]
                            }
                        ],
                        "is_spl_zero_interest": False,
                        "is_prescription": False,
                        "channel_exclusive_info": {
                            "source_id": 0,
                            "token": ""
                        },
                        "offerid": 0,
                        "supports_free_returns": False
                    }
                ],
                "tax_info": {
                    "use_new_custom_tax_msg": False,
                    "custom_tax_msg": "",
                    "custom_tax_msg_short": "",
                    "remove_custom_tax_hint": False
                },
                "tax_payable": 0,
                "shipping_id": 1,
                "shipping_fee_discount": 0,
                "shipping_fee": 5000000000,
                "order_total_without_shipping": 1000000000,
                "order_total": 6000000000,
                "buyer_remark": "",
                "ext_ad_info_mappings": []
            }
        ],
        "shipping_orders": [
            {
                "shipping_id": 1,
                "shoporder_indexes": [
                    0
                ],
                "selected_logistic_channelid": 8003,
                "selected_preferred_delivery_time_option_id": 0,
                "buyer_remark": "",
                "buyer_address_data": {
                    "addressid": 52404512,
                    "address_type": 0,
                    "tax_address": ""
                },
                "fulfillment_info": {
                    "fulfillment_flag": 64,
                    "fulfillment_source": "",
                    "managed_by_sbs": False,
                    "order_fulfillment_type": 2,
                    "warehouse_address_id": 0,
                    "is_from_overseas": False
                },
                "order_total": 6000000000,
                "order_total_without_shipping": 1000000000,
                "selected_logistic_channelid_with_warning": None,
                "shipping_fee": 5000000000,
                "shipping_fee_discount": 0,
                "shipping_group_description": "",
                "shipping_group_icon": "",
                "tax_payable": 0,
                "is_fsv_applied": False,
                "prescription_info": {
                    "images": None,
                    "required": False,
                    "max_allowed_images": 5
                }
            }
        ],
        "fsv_selection_infos": [],
        "buyer_info": {
            "share_to_friends_info": {
                "display_toggle": False,
                "enable_toggle": False,
                "allow_to_share": False
            },
            "kyc_info": None,
            "checkout_email": ""
        },
        "client_event_info": {
            "is_platform_voucher_changed": False,
            "is_fsv_changed": False
        },
        "buyer_txn_fee_info": {
            "title": "Biaya Penanganan",
            "description": "Besar biaya penanganan adalah Rp2.500 dari total transaksi.",
            "learn_more_url": "https://shopee.co.id/m/biaya-penanganan-indomaretisaku"
        },
        "disabled_checkout_info": {
            "description": "",
            "auto_popup": False,
            "error_infos": []
        },
        "can_checkout": True,
        "buyer_service_fee_info": {
            "learn_more_url": "https://shopee.co.id/m/biaya-layanan"
        },
        "__raw": {},
        "_cft": [
            1236587
        ],
        "captcha_version": 1,
        "device_info": {}
    })
