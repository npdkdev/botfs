from user import User
from bot import Bot
from placeorderdata import *
from order import AOAddOrder
from checkoutdata import PaymentInfo, PaymentChannel, PaymentChannelOptionInfo
from datetime     import datetime
from colorama     import Fore, Style, init
from time         import sleep, time
from datetime     import datetime, timedelta
from datetime import time as dtime
import os,sys
import pytz
import urllib3
from helper import place_order, Dpo, place_orderv2
import random
from json import dumps
from os.path import exists
class Npdk:
    shop: list
    user: list
    cookies: str
    devices: dict
    bot = None
    order:  dict = {}
    pay: dict = {}
    buyer: dict = {}
    shipping: dict = {}
    checkout: AOAddOrder
    session: dict = {}
    mode: int

    def __init__(self, cookie, devices,mode):
        self.cookies = cookie
        self.devices = devices
        self.mode = mode
        try:
            self.user = User.exec(self.cookies)
            self.bot = bot = Bot(self.user)
            self.buyer.update({"address_id":self.user.default_address.id,"service_fee":100000000,"fingerprint":self.devices['fingerprint']})
            print("WELCOME: ",self.user.name)
        except Exception as e:
            self.log(e)
            self.stop()

    def start(self):
        print("Masukan url barang yang akan dibeli")
        self.link = str(input("url product: "))
        #self.harga = int(input("Harga Max: "))
        #self.link = "https://shopee.co.id/Xiaomi-Redmi-Note-11-4-128-GB-Garansi-Resmi-i.228771622.16739283660"
        self.harga = 1000
        self.cekPrice = True
        try:
            self.item = self.bot.fetch_item_from_url(self.link)
            if not self.item.is_flash_sale:
                self.log("BUKAN PRODUCT FLASH SALE!!")
                self.stop()
            self.get_shipping(url=self.link)
            self.order.update({"shop_id":self.item.shop_id,"item_id":self.item.item_id})
            self.session.update({"link":self.link,"harga":self.harga,"cookie":self.cookies})
        except Exception as e:
            self.log(e)
            self.stop()
    def start_bot(self):
        tz = pytz.timezone('Asia/Jakarta')
        #start_time_bot = datetime.fromtimestamp(1670778315,tz)
        start_time_bot = datetime.fromtimestamp(self.item.flash_sale.start_time,tz)
        time_start_bot = start_time_bot - timedelta(milliseconds=650)
        second_coutndown = start_time_bot - timedelta(milliseconds=1500)
        print("WAKTU SEKARANG", datetime.now(tz).strftime("%H:%M:%S"))
        print("Waktu Flash Sale: ", start_time_bot.strftime("%H:%M:%S"))
        while True:
            if second_coutndown < datetime.now(tz): break
            print( "Menunggu Flash Sale... [{}]".format(str((time_start_bot - datetime.now(tz)).total_seconds())), end='\r')
        getItem = None
        nowPrice = self.buyer['price']
        if (time_start_bot - datetime.now(tz)).total_seconds() > 0:
            sleep((time_start_bot - datetime.now(tz)).total_seconds())
        print()
        print("BOT RUNNING")
        while True:
            if (start_time_bot - datetime.now(tz)).total_seconds() > 10:
                break
            getItem = self.bot.get_current_item(self.link,self.item.shop_id,self.item.item_id,self.buyer['model_id'])
            if "error_server" in getItem or "error_stock" in getItem:
                continue
            nowPrice = int(str(getItem['display_price'])[:-5])
            nowPrice2 = getItem['display_price']
            print(f"[{datetime.now(tz).strftime('%H:%M:%S:%f')}] HARGA NOW: {nowPrice} | HARGA MAX: {self.harga}", end="\r")
            if self.cekPrice and nowPrice <= self.harga:
                print(f"[PLACE_ORDER] HARGA: {nowPrice}")
                timest = int(time())
                orde = self.start_order(timest,nowPrice2)
                if orde is True:
                    break
                else:
                    orde = self.start_order(timest,nowPrice2)

        print("HARGA ASLI: ",int(str(self.item.price)[:-5]))
        print("HARGA AKHIR:", nowPrice)
    def setup_bot(self):
        ranint = (random.randint(1,2001) - 1000)
        ctimestamp = int(time() + ranint)
        self.shop = self.bot.get_shop_info(self.item.shop_id)
        self.start_bot()
    def start_order(self,timestamp,harga):
        data = Dpo(
            c={
                "price": harga,
                "shipping_fee": self.buyer['shipping_fee'],
                "service_fee": self.buyer['service_fee'],
                "txn_fee": self.buyer['txn_fee'],
            },
            s={
                "shopid": self.item.shop_id,
                "name": self.shop['name'],
                "cb_option": False,
                "is_official_shop": self.shop['is_official_shop'],
                "userid": "",
                "shop_tag": "",
            },
            pay={"chid":self.pay['channel_id'],"option":self.pay['option_info']},
            item={
                "id": self.item.item_id,
                "modelid": self.buyer['model_id'],
                "name": self.item.name,
                "model_name": self.buyer['model_name'],
                "image": self.item.image,
                "checkout": True,
                "catids": self.item.categories
            },
            shipping={"id":self.buyer['channel_id'],"address_id":self.buyer['address_id']}
        )
        
        dataa = place_orderv2(timestamp,data)
        asil = self.bot.place_order(dataa)
        if "error" in asil:
            asil = self.bot.place_order(dataa)
        if "checkoutid" in asil:
            print("BERHASIL CHECKOUT")
            return True
        else:
            print(asil)
            return False
    def save_session(self):
        with open('acc/'+np.user.name, 'w+') as svs:
            svs.write(dumps(self.session))
            svs.close()
    def set_session(self,data: dict):
        self.session.update(data)
    def set_order(self, order: dict):
        self.order.update(order)
    def set_pay(self, pay: dict):
        self.pay.update(pay)
        txn_fee = 0
        if "SHOPEEPAY" in pay['name']:txn_fee = 0
        elif "ALFAMART" in pay['name'] or "INDOMART_ISAKU" in pay['name']:txn_fee = 250000000
        elif "COD_BAYAR_DI_TEMPAT" in pay['name']:txn_fee = 0
        elif "TRANSFER_BANK" in pay['name']:
            txn_fee = 100000000
            if "TRANSFER_BANK_SEA_AUTO" in pay['option_name']:txn_fee = 0
        self.buyer.update({"txn_fee": txn_fee})
    def set_buyer(self, buyer: dict):
        self.buyer.update(buyer)
    def get_order(self):
        return self.order
    def get_checkout(self):
        checkout = self.bot.checkout_get_quick(data=self.order)
    def get_item(self):
        return self.item
    def get_bot(self):
        return self.bot
    def get_shipping(self, url=None):
        if url is not None:
            self.ship = self.bot.get_shipping(url, self.item, self.user)['ungrouped_channel_infos']
            return
        return self.ship
    
    def get_models(self):
        pass

    def log(self, msg: str, debug=False, level: int = 0):
        log_level = "[ERROR] " if level is 0 else "[WARNING]"
        print(log_level,msg)
        if debug:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
    def stop(self):
        print("BOT BERHENTI")
        exit(1)
